import axios from "axios"

import { API_BASE } from '../constants'

export const sendLogin = (username, password) => {
	const body = {username, password}
	return axios.post(API_BASE + "/api/v1/login", body)
}

export const createUser = (data) => {
	const body = {...data}
	return axios.post(API_BASE + "/api/v1/users", body)
}

export const verifyUsername = (username) => {
	return axios.get(`${API_BASE}/api/v1/users/check-username/${username}`)
}