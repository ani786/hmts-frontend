export const API_BASE = process.env.REACT_APP_API_BASE || "http://192.168.0.6:5000"
export const FADE_TIMEOUT = 1000
export const ZOOM_TIMEOUT = 500
export const LOGGEDIN = "loggedin"
export const LOGGEDOUT = "loggedout"
export const LOGINPENDING = "loginpending"
export const LOGINERROR = "loginerror"