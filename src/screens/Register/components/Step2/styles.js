import { makeStyles } from "@material-ui/core/styles";

export default makeStyles({
	mainDiv: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-between",
		alignItems: "center",
		paddingBottom: 20,
		width: "80%",
		textAlign: "center",
		"& > *": {
			margin: 10
		}
	},
	nextBtn: {
		textAlign: "center"
	}
});