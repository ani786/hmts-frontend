import { Card, Typography } from "@material-ui/core";
import React from "react";

import useStyles from "./styles";

const CardBase = ({ children, heading }) => {
	const classes = useStyles();

	return (
		<Card className={classes.cardBase}>
			<Typography
				variant="h4"
				display="block"
				gutterBottom
				className={classes.headingText}
			>
				{heading}
			</Typography>
			{children}
		</Card>
	);
};

export default CardBase;
