import React from "react";
import CardBase from "../CardBase/CardBase";
import {
	Button,
	FormControlLabel,
	FormLabel,
	Radio,
	RadioGroup,
	TextField,
} from "@material-ui/core";

import makeStyles from "./styles";

const Step3 = ({ formStep, formData, onDataChange, onSubmit }) => {
	const classes = makeStyles();

	if (formStep !== 3) {
		return null;
	}

	return (
		<CardBase heading="Enter basic detail">
			<div className={classes.mainDiv}>
				<TextField
					label="Name"
					variant="filled"
					value={formData.name}
					onChange={onDataChange}
					name="name"
					fullWidth
				/>
				<TextField
					label="Email"
					variant="filled"
					value={formData.email}
					onChange={onDataChange}
					name="email"
					fullWidth
				/>
				<div>
					<FormLabel>Gender</FormLabel>
					<RadioGroup
						aria-label="gender"
						name="gender"
						value={formData.gender}
						onChange={onDataChange}
					>
						<FormControlLabel
							value="female"
							control={<Radio />}
							label="Female"
						/>
						<FormControlLabel
							value="male"
							control={<Radio />}
							label="Male"
						/>
					</RadioGroup>
				</div>
				<TextField
					label="Relative Name"
					variant="filled"
					value={formData.relativeName}
					onChange={onDataChange}
					name="relativeName"
					fullWidth
				/>
				<TextField
					label="Relative Number"
					variant="filled"
					value={formData.relativeNumber}
					onChange={onDataChange}
					name="relativeNumber"
					fullWidth
				/>
				<Button
					variant="contained"
					color="primary"
					className={classes.deviceIDBtn}
					fullWidth={true}
					onClick={onSubmit}
				>
					Submit
				</Button>
			</div>
		</CardBase>
	);
};

export default Step3;
