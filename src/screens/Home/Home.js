import { Button, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles({
	main: {
		height: "100vh",
		width: "100vw",
		backgroundColor: "#1C2833",
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
	},
	heading: {
		color: "white",
		fontWeight: 600,
		textAlign: "center",
		fontFamily: "Montserrat",
	},
	button: {
		display: "flex",
		justifyContent: "space-evenly",
		width: "20%",
		marginTop: 30
	}
});

const Home = (props) => {
	const classes = useStyles();
	return (
		<div className={classes.main}>
			<Typography variant="h2" className={classes.heading}>
				Health Monitoring and <br />
				Tracking System
			</Typography>
			<div className={classes.button}>
				<Button variant="contained" color="primary" href="/login">
					Login
				</Button>
				<Button variant="contained" color="primary" href="/register">
					Register
				</Button>
			</div>
		</div>
	);
};

export default Home;
