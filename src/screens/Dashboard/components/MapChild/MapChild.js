import { useMap } from "react-leaflet";

function MapChild({lat, lon}) {
	const map = useMap()
	map.panTo([lat, lon])
	
	return null;
}

export default MapChild;
