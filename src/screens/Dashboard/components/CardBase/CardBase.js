import { Card, CardContent, CardMedia, Typography } from "@material-ui/core";
import React from "react";

import makeStyles from "./styles";

const CardBase = ({imgSrc, heading, caption, value}) => {
	const classes = makeStyles();

	return (
		<Card className={classes.root} elevation={10}>
			<CardMedia
				classes={{ img: classes.img }}
				component="img"
				width="250"
				height="250"
				image={imgSrc}
			/>
			<CardContent className={classes.cardContent}>
				<Typography variant="h4" gutterBottom>
					{heading}
				</Typography>
				<Typography variant="subtitle1">
					{caption}
				</Typography>
				<Typography variant="h3">
					{value}
				</Typography>
			</CardContent>
		</Card>
	);
};

export default CardBase;

