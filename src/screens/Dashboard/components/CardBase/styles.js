import { makeStyles } from "@material-ui/core/styles";

export default makeStyles({
	root: {
		transform: "scale(1)",
		transition: "0.2s",
		transitionTimingFunction: "ease-out",
		"&:hover": {
			transform: "scale(1.02)"
		},
	},
	img: {
		objectFit: "contain",
	},
	cardContent: {
		textAlign: "center",
	},
});
