import React, { useEffect, useState } from "react";
import {
	Button,
	Container,
	Grid,
	makeStyles,
	Typography,
} from "@material-ui/core";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import CardBase from "./components/CardBase/CardBase";
import MapChild from "./components/MapChild/MapChild";
import { getDeviceData } from "../../api/deviceAPI";
import Loading from "../Loading/Loading";
import ZoomAdvanced from "../../util/ZoomAdvanced/ZoomAdvanced";

import thermometer from "../../assets/images/thermometer.jpg";
import heart from "../../assets/images/heart.png";
import { useSelector } from "react-redux";

const useStyles = makeStyles({
	grid: {
		justifyContent: "center",
	},
	mapRoot: {
		margin: "50px 0px",
	},
	map: {
		height: 500,
	},
	headingRoot: {
		textAlign: "center",
		position: "relative",
	},
	heading: {
		display: "inline",
	},
	logoutBtn: {
		position: "absolute",
		right: 0,
		top: "30%",
	},
});

const Dashboard = () => {
	const classes = useStyles();

	const [device, changeDeviceData] = useState({
		lat: 0,
		lon: 0,
		temp: 0,
		pulse: 0,
	});
	const [loading, setLoading] = useState(true);
	const user = useSelector((state) => state.user);

	useEffect(() => {
		const fetchData = async () => {
			try {
				const res = await getDeviceData(user.token);
				changeDeviceData(res.data);
				setLoading(false);
				setInterval(async () => {
					const res = await getDeviceData(user.token);
					changeDeviceData(res.data);
				}, 5000);
			} catch (error) {}
		};
		fetchData();
	}, [user.token]);

	return (
		<>
			{loading ? (
				<Loading />
			) : (
				<Container className={classes.root}>
					<div className={classes.headingRoot}>
						<Typography
							variant="h1"
							className={classes.heading}
							gutterBottom
						>
							Welcome {user.username}
						</Typography>
						<Button
							variant="contained"
							color="primary"
							className={classes.logoutBtn}
							href="/logout"
						>
							Logout
						</Button>
					</div>
					<Grid container spacing={2} className={classes.grid}>
						<ZoomAdvanced number={0}>
							<Grid item sm={6}>
								<CardBase
									imgSrc={thermometer}
									heading="Body Temperature"
									caption="Current body temperature:"
									value={Math.round(device.temp)}
								/>
							</Grid>
						</ZoomAdvanced>
						<ZoomAdvanced number={1}>
							<Grid item sm={6}>
								<CardBase
									imgSrc={heart}
									heading="Heart Rate"
									caption="Current heart rate:"
									value={Math.round(device.pulse)}
								/>
							</Grid>
						</ZoomAdvanced>
					</Grid>
					<ZoomAdvanced number={2}>
						<div className={classes.mapRoot}>
							<Typography
								variant="h3"
								align="center"
								gutterBottom
							>
								Current Location
							</Typography>
							<MapContainer
								center={[device.lat, device.lon]}
								zoom={17}
								scrollWheelZoom={false}
								className={classes.map}
							>
								<MapChild lat={device.lat} lon={device.lon} />
								<TileLayer
									attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
									url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
								/>
								<Marker position={[device.lat, device.lon]}>
									<Popup>Current Location</Popup>
								</Marker>
							</MapContainer>
						</div>
					</ZoomAdvanced>
				</Container>
			)}
		</>
	);
};

export default Dashboard;
