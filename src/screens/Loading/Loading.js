import React from "react";

import './styles.css'
function Loading() {
	return (
		<div className="main">
			<div className="loader loader-black loader-2"></div>
		</div>
	);
}

export default Loading;
