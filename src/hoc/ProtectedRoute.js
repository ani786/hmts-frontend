import React from 'react'
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from "react-redux";
import {LOGGEDIN} from "../constants"

const ProtectedRoute = ({children, ...rest}) => {
	const user = useSelector(state => state.user)
	let isLoggedIn = false

	if(user.loginStatus === LOGGEDIN) {
		isLoggedIn = true
	}
	return (
		<>
			{isLoggedIn ? 
				<Route {...rest}>
					{children}
				</Route>
			: 
				<Redirect to="/login" />
			}
		</>
	);
};

export default ProtectedRoute;
